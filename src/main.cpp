#include "raylib.h"

#include "imgui.h"
#include "rlImGui.h"

int main() {
    int width  = 800;
    int height = 600;

    InitWindow(width, height, "Level Editor");

    SetTargetFPS(60);

    rlImGuiSetup(true);
    while(!WindowShouldClose()) {
        BeginDrawing();
        ClearBackground(WHITE);

        rlImGuiBegin();
        bool open = true;
        ImGui::ShowDemoWindow(&open);
        rlImGuiEnd();


        EndDrawing();
    }

    rlImGuiShutdown();
    CloseWindow();

    return 0;
}
